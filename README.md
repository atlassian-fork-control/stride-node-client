
**New to Stride?**  We suggest  following the - [Stride Getting Started](https://developer.atlassian.com/cloud/stride/getting-started/) guide that uses the Stride client. We also have a full-featured reference app part of the same project that demonstrating all (well, almost all ) the features that comprise of the Stride ecosystem.

 - [Reference App](https://bitbucket.org/atlassian/stride-apps-reference/)
 - [Create An App](https://developer.atlassian.com/apps/)

The Stride client is quite simple.  After initialization, your app will use the Stride client to generate a token and send a request to our [REST API](https://developer.atlassian.com/cloud/stride/rest/).  

## Installation 

```npm install stride-node-client```


## Prerequisite

You must install the app in the conversation. Apps are granted permission only for conversations they are installed in. 


## Usage


```javascript

//require-in the lib
const Stride = require('stride-node-client');

// Save environment variables
const { CLIENT_ID, CLIENT_SECRET, NODE_ENV } = process.env;

// Instantiate client
const stride = new Stride({
	 CLIENT_ID: CLIENT_ID,
	 CLIENT_SECRET: CLIENT_SECRET,
	 NODE_ENV: 'production',
	 });
	 


// Setting Context: These values can be found in your URL when you are in a conversation: https://app.stride.com/cloudId/chat/conversationId 
const conversationId = "";
const cloudId = "";



// Send Message To Conversation
await stride.api.messages.sendMessage(cloudId, conversationId, {  
  body: `Hello World!`,  
  headers: { 'Content-Type': 'text/plain', accept: 'application/json' },  
});

// Fetch Conversation Roster
const { roster } = await stride.api.conversations.getRoster(cloudId, conversationId);

// Archive Conversation
await stride.api.conversations.archive(cloudId, conversationId, { body: {} });

// Unarchive Conversation
await stride.api.conversations.unarchive(cloudId, conversationId);


```





function isObject(value) {
  return Object(value) === value;
}
/**
 * @return {array<string>} Array of mentionId's
 * @description
 *
 * A recursive function that crawls the ADF message and returns an array of userId's mentioned
 **/
function getMentionIdsFromADF(document) {
  if (isObject(document)) {
    if (document.type === 'mention' && document.attrs) {
      return [document.attrs.id];
    } else {
      let outputArray = [];
      for (let prop in document) {
        if (!document.hasOwnProperty(prop)) continue;
        let propValue = document[prop];
        if (isObject(propValue) || Array.isArray(propValue)) {
          outputArray = [...outputArray, ...getMentionIdsFromADF(propValue)];
        }
      }
      return outputArray;
    }
  } else if (Array.isArray(document)) {
    return document.reduce(
      (accumulator, currentItem) => [...accumulator, ...getMentionIdsFromADF(currentItem)],
      []
    );
  }
  return [];
}

/**
 * @return {boolean} true = user id found. false = user id not found.
 * @description
 *
 * A function that crawls the ADF message and returns true if the user id is mentioned
 **/
function isUserIdMentionedInMessage(adfMessage, id) {
  return getMentionIdsFromADF(adfMessage).indexOf(id) > 0;
}

module.exports = {
  getMentionIdsFromADF,
  isUserIdMentionedInMessage,
};
